# if-helios

Esse repositório contém diversas ferramentas para extrair e incluir
informações do sistema helios voting do ifce [acessível aqui](
https://eleicoes.ifce.edu.br).

Atualmente ele faz um parsing das páginas web do sistema e extrai
as informações, no entanto, o ideal seria usar diretamente
o módulo do Django para extrair tais informações.

### dependências

Esse projeto depende de ferramentas do sistema e bibliotecas python.

A nível de sistema é necessário instalar:

* wkhtmltopdf (para gerar os pdfs)

Já as bibliotecas python necessárias são:

* pdfkit
* beautifulsoup4

Infelizmente não estamos utilizando virtualenv nesse repositório,
portanto, tem que instalar o pacote na mão.


### estrutura do projeto

```
.
├── auth.conf.example
├── election.conf
├── eleicao
├── ifhelios.py
├── ifutils.py
├── readme.md
├── templates
├── todos-alunos.csv (não está no repositório)
└── todos-servidores.csv (não está no repositório)
```

os arquivos `todos-alunos.csv` e `todos-servidores.csv` não estão no repositório
pois contém dados sensíveis. Mas suas estrutura são:

todos-alunos.csv
```
CAMPUS_LOTACAO,CURSO,NOME,MATRICULA,EMAIL
```

todos-servidores.csv
```
SEGMENTO,NOME,CAMPUS_LOTACAO,SIAPE,EMAIL
```

os arquivos `ifutils.py` e `ifhelios.py` são uma tentativa de unificar em um
só pacote os diversos scripts python do diretório eleicao.

Dentro do diretório `eleicao` temos diversos scripts para gerenciar as eleições,
tais como:

* criar diversas urnas em lote (`create_election.py`)
* contar quantidade de votos em cada uma das urnas (`count_cast_votes.py`)
* extender eleições caso seja necessãrio (`extend_election.py`)
* exportar resultados das eleições (`extend_election.py`)

Entre outros scripts que buscam auxiliar o operador da eleição.

#!/bin/env python

import sys
import json
import uuid
import requests
from bs4 import BeautifulSoup
from dados import elections

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Content-Type': 'application/x-www-form-urlencoded'
}

# alterar urls para apontar para sistema em producao
auth_url = 'https://eleicoes.ifce.edu.br/auth/ldap/login'
new_election_url = 'https://eleicoes.ifce.edu.br/helios/elections/new'
elections_url = 'https://eleicoes.ifce.edu.br/helios/elections/administered'

extend_election_url_inc = 'https://eleicoes.ifce.edu.br/helios/elections/{0}/extend'

config_filename = '../auth.conf'
with open(config_filename, 'r') as config_file:
    auth_data = json.load(config_file)

extending_data = {
    'csrf_token': '',
    'voting_extended_until': '09/10/2020 17:00'
}

def get_session():
    client = requests.session()
    return client

def authenticate(client):
    r = client.post(auth_url, data = auth_data)
    return r, client

def get_csrf_token(client):
    r = client.get(new_election_url)
    soup = BeautifulSoup(r.text, 'lxml')
    csrf_token = soup.find('input', attrs = {'name': 'csrf_token'})['value']
    return csrf_token

def get_elections(client, url):
    r = client.get(url)
    return r

def get_elections_id(r):
    all_ids = []
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    for link in links:
        if link['href'].endswith('view'):
            all_ids.append(link['href'].split('/')[-2])
    return all_ids

def post_extension(client, extending_data, extend_election_url):
    r = client.post(extend_election_url, data = extending_data)
    return r

def post_eligibility(client, eligibility_url, eligibility_data):
    client.post(eligibility_url, data = eligibility_data)

if __name__ == "__main__":
    client = get_session()
    r, client = authenticate(client)
    csrf_token = get_csrf_token(client)
    # prepare election_data

    r = get_elections(client, elections_url)
    all_ids = get_elections_id(r)
    extending_data['csrf_token'] = csrf_token
    for e_id in all_ids:
        url = extend_election_url_inc.format(e_id)
        r = client.post(url, data = extending_data)
        if r.status_code == 200:
            print('eleicao extendida para 09/10/2020 17:00')

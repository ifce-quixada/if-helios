#!/usr/bin/env python

import os
import csv
import sys
import string
import argparse

from ifutils import *

campi = ['IFCE-ACARAU', 'IFCE-ACOPIARA', 'IFCE-ARACATI', 'IFCE-BATURITE', 'IFCE-BOAVIAGEM',
        'IFCE-CAMOCIM', 'IFCE-CANINDE', 'IFCE-CAUCAIA', 'IFCE-CEDRO', 'IFCE-CRATEUS', 'IFCE-CRATO',
        'IFCE-FORTALEZA', 'IFCE-GUARAMIRANGA', 'IFCE-HORIZONTE', 'IFCE-IGUATU', 'IFCE-ITAPIPOCA',
        'IFCE-JAGUARIBE', 'IFCE-JAGUARUANA', 'IFCE-JUAZEIRO', 'IFCE-LIMOEIRO', 'IFCE-MARACANAU',
        'IFCE-MARANGUAPE', 'IFCE-MORADA', 'IFCE-PARACURU', 'IFCE-PECEM', 'IFCE-QUIXADA', 'IFCE-REITORIA',
        'IFCE-SOBRAL', 'IFCE-TABULEIRO', 'IFCE-TAUA', 'IFCE-TIANGUA', 'IFCE-UBAJARA', 'IFCE-UMIRIM']

def split_student_file(options):
    if (is_csv(options.file)):
        print('File appears not to be a CSV file')
        sys.exit(-1)

    content_list = get_csv_content(options.file)
    unique_content_list = get_unique_content(content_list)
    for campus in campi:
        content = get_content_by_campus(unique_content_list, campus)
        save_file(content, campus)

def split_worker_file(options):
    if (is_csv(options.file)):
        print('File appears not to be a CSV file')
        sys.exit(-1)

    content_list = get_csv_content(options.file)
    unique_content_list = get_unique_content(content_list)
    for campus in campi:
        content = get_content_by_campus(unique_content_list, campus, category = 'worker')
        docente_content = [[i[0], i[1], i[2], i[3]] for i in content if i[-1] == 'DOCENTE']
        tae_content = [[i[0], i[1], i[2], i[3]] for i in content if i[-1] == 'TÉCNICO ADMINISTRATIVO']
        save_file(docente_content, campus, category='docente')
        save_file(tae_content, campus, category='tae')

def export_result(options):
    auth_data = get_auth_data(options.config_file)

    client = get_session()
    r, client = authenticate(client, auth_data)
    csrf_token = get_csrf_token(client)

    r = get_elections(client)
    all_ids = get_elections_id(r)
    print(all_ids)
    for idx, e_id in enumerate(all_ids):
        url = view_result_url_inc.format(e_id)
        r = client.get(url)
        if r.status_code == 200:
            try:
                election_name = get_election_name(r)
                table = get_result_table(r)
                es_1, es_2, es_3 = get_election_schedule(r)
                prepare_pdf(
                    election_name.contents[0],
                    es_1, es_2, es_3,
                    url,
                    table)
            except Exception as e:
                print('something went wrong, the error was:', e)

if __name__ == "__main__":
    # TODO: load commands
    create_output_structure()

    parser = argparse.ArgumentParser(
            description='Ferramenta para gerenciar sistema de votacao Helios')
    subparsers = parser.add_subparsers()

    # create split campi subcommand
    parser_set_environment = subparsers.add_parser('gerar-lista-aluno', help='Gera nova listagem para alunos')
    parser_set_environment.add_argument('--file', '-f', help='Arquivo csv contendo todos os alunos da instituicao', type=str)
    parser_set_environment.set_defaults(func=split_student_file)

    # create split campi subcommand
    parser_set_environment = subparsers.add_parser('gerar-lista-servidor', help='Gera nova listagem para servidores - tae e docentes')
    parser_set_environment.add_argument('--file', '-f', help='Arquivo csv contendo todos os servidores da instituicao', type=str)
    parser_set_environment.set_defaults(func=split_worker_file)

    # create export result subcommand
    parser_set_environment = subparsers.add_parser('exportar-resultado', help='Gera arquivos PDFs com os resultados da eleicao')
    parser_set_environment.add_argument('--config-file', '-c', help='Arquivo de configuracao contendo informacao de autenticacao', type=str)
    parser_set_environment.set_defaults(func=export_result)

    if len(sys.argv) <= 1:
        sys.argv.append('--help')

    options = parser.parse_args()
    options.func(options)

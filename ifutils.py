#!/usr/bin/env python

import re
import os
import sys
import csv
import json
import jinja2
import pdfkit
import requests

from bs4 import BeautifulSoup

base_url = 'https://eleicoes.ifce.edu.br'
auth_url = base_url + '/auth/ldap/login'
elections_url = base_url + '/helios/elections/administered'
new_election_url = base_url + '/helios/elections/new'

save_question_url_inc = base_url + '{0}/{1}'
view_result_url_inc = base_url + '/helios/elections/{0}/view'

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Content-Type': 'application/x-www-form-urlencoded'
}

config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
template_file = "templates/index.j2"

non_url_safe = ['"', '#', '$', '%', '&', '+',
    ',', '/', ':', ';', '=', '?',
    '@', '[', '\\', ']', '^', '`',
    '{', '|', '}', '~', "'"]

translate_table = {ord(char): u'' for char in non_url_safe}
non_url_safe_regex = re.compile(
    r'[{}]'.format(''.join(re.escape(x) for x in non_url_safe)))

def slugify(text):
    text = text.translate(translate_table)
    text = u'_'.join(text.split())
    return text

def is_csv(filename):
    try:
        csv_file = open(filename, 'r', encoding='utf-8')
    except FileNotFoundError as e:
        print(e)
        sys.exit(-1)
    try:
        csv_content = csv_file.read(2048)
    except UnicodeDecodeError as e:
        csv_file.close()
        print(e)
        return 1
    try:
        dialect = csv.Sniffer().sniff(csv_content, delimiters=',')
        return 0
    except Exception as e:
        return 1
    try:
        csv.Sniffer().has_header(csv_content)
        return 0
    except csv.Error as e:
        return 1

def create_output_structure():
    base_path = os.path.dirname(__file__) + '/{0}/{1}'
    root_dir = 'arquivos-gerados'
    child_dir = ['aluno', 'servidor', 'resultado']
    categories_dir = ['tae', 'docente']

    for child in child_dir:
        try:
            os.makedirs(base_path.format(root_dir, child))
        except FileExistsError:
            pass
        if child == 'servidor':
            for category in categories_dir:
                try:
                    os.makedirs(base_path.format(root_dir, child) + '/' + category)
                except FileExistsError:
                    pass

def get_csv_content(filename):
    with open(filename, 'r', encoding='utf-8') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        content_list = []
        for k, row in enumerate(csv_reader):
            if k == 0:
                csv_keys = row
                continue
            content_list.append(dict(zip(csv_keys, row)))
    return content_list

def get_unique_content(content_list):
    unique_sets = set(frozenset(d.items()) for d in content_list)
    unique_dicts = [dict(s) for s in unique_sets]
    return unique_dicts

def get_content_by_campus(unique_content_list, campus, category = None):
    if campus == 'IFCE-IGUATU':
        print(len(unique_content_list))
    if category == 'worker':
        content = [[i['SIAPE'], i['EMAIL'].split('-')[0].strip(), i['CAMPUS_LOTACAO'], i['NOME'], i['SEGMENTO']] for i in unique_content_list if i['CAMPUS_LOTACAO'] == campus]
    else:
        content = [[i['MATRICULA'], i['EMAIL'], i['CAMPUS_LOTACAO'], i['NOME']] for i in unique_content_list if i['CAMPUS_LOTACAO'] == campus]
    return content

def handle_email(email, campus):
    base_email = 'celocal.{0}@ifce.edu.br'
    if len(email) == 0:
        return base_email.format(campus.split('-')[1].lower())

    list_of_emails = re.findall('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', email)
    aux = [k for k in list_of_emails if k]
    try:
        return aux[-1]
    except Exception as e:
        print('falhou', e, campus, email)

    for idx, e in enumerate(list_of_emails):
        if '/' in e:
            tmp = e.split('/')
            aux = [i for i in tmp if i]
            return aux[-1].strip()

        if '\\' in e:
            tmp = e.split('\\')
            aux = [i for i in tmp if i]
            return aux[-1].strip()

        if ' / ' in e:
            print('---->', campus, '--', e)
            tmp = e.split(' / ')
            aux = [i for i in tmp if i]
            return aux[-1].strip()

        if '   /   ' in e:
            tmp = e.split('   /  ')
            aux = [i for i in tmp if i]
            return aux[-1].strip()

    return email.strip()

def save_file(content, campus, category = None):
    filename = '{0}_with_campus.csv'.format(campus.split('-')[1].lower())
    if category == None:
        filename_path = os.path.join(os.path.dirname(__file__), 'arquivos-gerados', 'aluno', filename)
    else:
        filename_path = os.path.join(os.path.dirname(__file__), 'arquivos-gerados', 'servidor', category, filename)
    with open(filename_path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(content)

def get_auth_data(config = None):
    if not config:
        filename = 'auth.conf'
    else:
        filename = config

    base_path = os.path.dirname(__file__)
    config_filename = os.path.join(base_path, filename)
    with open(config_filename, 'r') as config_file:
        auth_data = json.load(config_file)
        return auth_data

def get_election_data(config = None):
    if not config:
        filename = 'election.conf'
    else:
        filename = config

    base_path = os.path.dirname(__file__)
    election_filename = os.path.join(base_path, filename)
    with open(config_filename, 'r') as election_file:
        election_data = json.load(config_file)
        return election_data

def get_session():
    client = requests.session()
    return client

def authenticate(client, auth_data):
    r = client.post(auth_url, data = auth_data)
    return r, client

def get_csrf_token(client):
    r = client.get(new_election_url)
    soup = BeautifulSoup(r.text, 'lxml')
    csrf_token = soup.find('input', attrs = {'name': 'csrf_token'})['value']
    return csrf_token

def get_elections(client):
    r = client.get(elections_url)
    return r

def post_election(client, election_data):
    r = client.post(new_election_url, data = election_data)
    return r

def get_elections_id(r):
    all_ids = []
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    for link in links:
        if link['href'].endswith('view'):
            all_ids.append(link['href'].split('/')[-2])
    return all_ids

def get_result_table(r):
    soup = BeautifulSoup(r.text, 'lxml')
    table = soup.find('table', attrs = {'class': 'pretty'})
    table['class'] = ['table', 'is-bordered', 'is-striped']
    table['style'] = ''
    thead = soup.new_tag('thead')
    tr = soup.new_tag('tr')
    th = soup.new_tag('th')
    th.string = 'Número de votos'
    tr.insert(0, th)
    th = soup.new_tag('th')
    th.string = 'Candidatos'
    tr.insert(0, th)
    thead.insert(0, tr)
    table.insert(0, thead)

    all_tds = table.findAll('td')
    for td in all_tds:
        if (td.attrs['style'] == 'padding-right:80px;font-weight:bold;'):
            td.attrs['style'] = 'font-weight:bold;'
            td.attrs['align'] = 'left'
        if (td.attrs['style'] == 'padding-right:80px;'):
            td.attrs['align'] = 'left'
            del td.attrs['style']

    return table

def get_election_name(r):
    soup = BeautifulSoup(r.text, 'lxml')
    election_name = soup.find('h3', attrs={'class': 'title'})
    return election_name

def get_election_schedule(r):
    soup = BeautifulSoup(r.text, 'lxml')
    election_schedule = soup.find('div', attrs={'class': 'panel-body'})
    try:
        es_1 = election_schedule.contents[0].strip()
        es_2 = election_schedule.contents[2].strip()
        es_3 = election_schedule.contents[4].strip() + ' ' + election_schedule.contents[6].strip()
    except Exception as e:
        print(e)
    return es_1, es_2, es_3

def prepare_pdf(election_name, es_1, es_2, es_3, url, table_content):
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(template_file)
    outputText = template.render(
            election_name=election_name,
            es_1=es_1,
            es_2=es_2,
            es_3=es_3,
            url=url,
            table_content=table_content)
    short_name = slugify(election_name).lower()

    pdfkit.from_string(
            outputText,
            os.path.join('arquivos-gerados/resultado', short_name + '.pdf'),
            configuration=config,
            options= {
                'page-size': 'A4',
                'margin-top': '0.25in',
                'margin-right': '1in',
                'margin-left': '1in',
                'margin-bottom': '0.25in',
                'encoding': 'UTF-8'
            })

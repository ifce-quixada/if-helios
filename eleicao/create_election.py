#!/bin/env python

import sys
import json
import uuid
import requests

from faker import Faker
from bs4 import BeautifulSoup
from dados import elections, reitor_candidates


#ENV='dev'
ENV='prod'
CATEGORIES = ['Discente', 'Docente', 'Técnico-Administrativo']

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Content-Type': 'application/x-www-form-urlencoded'
}
# alterar urls para apontar para sistema em producao
base_url =  'https://h-helios.ifce.edu.br'
#base_url =  'https://eleicoes.ifce.edu.br'
auth_url = base_url + '/auth/ldap/login'
new_election_url = base_url + '/helios/elections/new'
save_question_url_inc = base_url + '{0}/{1}'
url_inc = base_url + '{0}/{1}'

config_filename = '../auth.conf'
with open(config_filename, 'r') as config_file:
    auth_data = json.load(config_file)

eligibility_data = { 'eligibility': 'privatereg', 'csrf_token': '' }
base_election = {
    'short_name': '',
    'name': '',
    'description': '',
    'use_voter_aliases': 'on',
    'private_p': 'on',
    'voto_unico': 'on',
    'help_email': 'suporte.eleicoes@ifce.edu.br',
    'voting_starts_at': '13/11/2020 08:00',
    'voting_ends_at': '13/11/2020 20:00',
    'election_type': 'election'
}

base_questions  = {
    'short_name': '',
    'question': '',
    'min': 1,
    'max': 1,
    'answers': [],
    'answer_urls': [],
    'choice_type': 'approval',
    'tally_type': 'homomorphic',
    'result_type': 'absoluta'
}

def get_session():
    client = requests.session()
    return client

def authenticate(client):
    r = client.post(auth_url, data = auth_data)
    return r, client

def get_csrf_token(client):
    r = client.get(new_election_url)
    soup = BeautifulSoup(r.text, 'lxml')
    csrf_token = soup.find('input', attrs = {'name': 'csrf_token'})['value']
    return csrf_token

def post_election(client, election_data):
    r = client.post(new_election_url, data = election_data)
    return r

def get_url(r):
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    for link in links:
        if link['href'].endswith('questions'):
            question_link = link['href'].split('/')[:-1]
            save_questions_url = url_inc.format('/'.join(question_link), 'save_questions')
            eligibility_url = url_inc.format('/'.join(question_link), 'voters/eligibility')
            voters_url = url_inc.format('/'.join(question_link), 'voters/upload')
            return save_questions_url, eligibility_url, voters_url
    return -1

def post_questions(client, questions_data, save_questions_url):
    r = client.post(save_questions_url, data = questions_data)
    return r

def post_eligibility(client, eligibility_url, eligibility_data):
    client.post(eligibility_url, data = eligibility_data)

if __name__ == "__main__":
    client = get_session()
    r, client = authenticate(client)
    csrf_token = get_csrf_token(client)
    # prepare election_data

    short_name = 'ele-{0}'
    if ENV == 'prod':
        name = 'Eleição para Reitor(a) e Diretor(a) - {0} - {1}'
    else:
        name = '[TESTE] Eleição para Reitor(a) e Diretor(a) - {0} - {1}'
    questions = ['Quem deve ser o(a) Diretor(a) do Campus?', 'Quem deve ser o(a) Reitor(a) da Instituição?']

    for category in CATEGORIES:
        for election in elections:
            questions_json = []
            uuid_hex = uuid.uuid4().hex[:6]
            print("########## cadastrando {0}-{1} ##########".format(election['short_name_campus'], uuid_hex))
            election_data = base_election
            election_data['csrf_token'] = csrf_token
            # alterar 'tae' para a categoria certa
            election_data['short_name'] = short_name.format(uuid_hex)
            # alterar 'TAE' para a categoria certa
            election_data['name'] = name.format(election['campus'], category)
            r = post_election(client, election_data)
            save_questions_url, eligibility_url, voters_url = get_url(r)
            if save_questions_url == -1:
                print('algum erro aconteceu')
                sys.exit(-1)

            if ENV == 'prod':
                qd = base_questions.copy()
                qd['question'] = questions[0]
                qd['short_name'] = short_name.format(uuid.uuid4().hex[:6])
                qd['answers'] = election['candidates']['diretor']
                qd['answer_urls'] = [''] * len(election['candidates']['diretor'])
                questions_json.append(qd)

                qd = base_questions.copy()
                qd['question'] =  questions[1]
                qd['short_name'] = 'q-' + short_name.format(uuid.uuid4().hex[:6])
                qd['answers'] = reitor_candidates
                qd['answer_urls'] = [''] * len(reitor_candidates)
                questions_json.append(qd)
                questions_data = {
                    'csrf_token':  csrf_token,
                    'questions_json': json.dumps(questions_json)
                }
            else:
                fake = Faker('pt_BR')
                diretor_candidates = [fake.name() for _ in range(3)]
                reitor_candidates = [fake.name() for _ in range(4)]

                qd = base_questions.copy()
                qd['question'] = '[TESTE] - ' + questions[0]
                qd['short_name'] = short_name.format(uuid.uuid4().hex[:6])
                qd['answers'] = diretor_candidates
                qd['answer_urls'] = [''] * len(diretor_candidates)
                questions_json.append(qd)
                questions_data = {
                    'csrf_token':  csrf_token,
                    'questions_json': json.dumps(questions_json)
                }

                qd = base_questions.copy()
                qd['question'] = '[TESTE] - ' + questions[1]
                qd['short_name'] = short_name.format(uuid.uuid4().hex[:6])
                qd['answers'] = reitor_candidates
                qd['answer_urls'] = [''] * len(diretor_candidates)
                questions_json.append(qd)
                questions_data = {
                    'csrf_token':  csrf_token,
                    'questions_json': json.dumps(questions_json)
                }

            r = post_questions(client, questions_data, save_questions_url)
            print('questions added?:', r.text, qd['short_name'])
            election_data['csrf_token'] = csrf_token
            r = post_eligibility(client, eligibility_url, eligibility_data)

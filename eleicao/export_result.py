#!/bin/env python

import os
import sys
import json
import uuid
import jinja2
import pdfkit
import requests
from bs4 import BeautifulSoup
from dados import elections

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Content-Type': 'application/x-www-form-urlencoded'
}

config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
template_file = "templates/index.j2"

# alterar urls para apontar para sistema em producao
auth_url = 'https://eleicoes.ifce.edu.br/auth/ldap/login'
new_election_url = 'https://eleicoes.ifce.edu.br/helios/elections/new'
elections_url = 'https://eleicoes.ifce.edu.br/helios/elections/administered'

view_result_url_inc = 'https://eleicoes.ifce.edu.br/helios/elections/{0}/view'

config_filename = '../auth.conf'
with open(config_filename, 'r') as config_file:
    auth_data = json.load(config_file)

release_result_data = {
    'csrf_token': ''
}

def get_session():
    client = requests.session()
    return client

def authenticate(client):
    r = client.post(auth_url, data = auth_data)
    return r, client

def get_csrf_token(client):
    r = client.get(new_election_url)
    soup = BeautifulSoup(r.text, 'lxml')
    csrf_token = soup.find('input', attrs = {'name': 'csrf_token'})['value']
    return csrf_token

def get_elections(client, url):
    r = client.get(url)
    return r

def get_elections_id(r):
    all_ids = []
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    for link in links:
        if link['href'].endswith('view'):
            all_ids.append(link['href'].split('/')[-2])
    return all_ids

def get_result_table(r):
    soup = BeautifulSoup(r.text, 'lxml')
    table = soup.find('table', attrs = {'class': 'pretty'})
    table['class'] = ['table', 'is-bordered', 'is-striped']
    table['style'] = ''
    thead = soup.new_tag('thead')
    tr = soup.new_tag('tr')
    th = soup.new_tag('th')
    th.string = 'Número de votos'
    tr.insert(0, th)
    th = soup.new_tag('th')
    th.string = 'Candidatos'
    tr.insert(0, th)
    thead.insert(0, tr)
    table.insert(0, thead)

    all_tds = table.findAll('td')
    for td in all_tds:
        if (td.attrs['style'] == 'padding-right:80px;font-weight:bold;'):
            td.attrs['style'] = 'font-weight:bold;'
            td.attrs['align'] = 'left'
        if (td.attrs['style'] == 'padding-right:80px;'):
            td.attrs['align'] = 'left'
            del td.attrs['style']

    return table

def get_election_name(r):
    soup = BeautifulSoup(r.text, 'lxml')
    election_name = soup.find('h3', attrs={'class': 'title'})
    return election_name

def get_election_schedule(r):
    soup = BeautifulSoup(r.text, 'lxml')
    election_schedule = soup.find('div', attrs={'class': 'panel-body'})
    try:
        es_1 = election_schedule.contents[0].strip()
        es_2 = election_schedule.contents[2].strip()
        es_3 = election_schedule.contents[4].strip() + ' ' + election_schedule.contents[6].strip()
    except Exception as e:
        print(e)
    return es_1, es_2, es_3

def post_eligibility(client, eligibility_url, eligibility_data):
    client.post(eligibility_url, data = eligibility_data)

def prepare_pdf(election_name, es_1, es_2, es_3, url, table_content):
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(template_file)
    outputText = template.render(
            election_name=election_name,
            es_1=es_1,
            es_2=es_2,
            es_3=es_3,
            url=url,
            table_content=table_content)
    short_name = election_name.replace(' ', '_').lower()
    #html_file = open(short_name + '.html', 'w')
    #html_file.write(outputText)
    #html_file.close()
    #pdfkit.from_file(short_name + '.html', short_name + '.pdf')
    pdfkit.from_string(
            outputText,
            os.path.join('pdfs/', short_name + '.pdf'),
            configuration=config,
            options= {
                'page-size': 'A4',
                'margin-top': '0.25in',
                'margin-right': '1in',
                'margin-left': '1in',
                'margin-bottom': '0.25in',
                'encoding': 'UTF-8'
            })
    print(short_name)


if __name__ == "__main__":
    client = get_session()
    r, client = authenticate(client)
    csrf_token = get_csrf_token(client)

    r = get_elections(client, elections_url)
    all_ids = get_elections_id(r)
    for idx, e_id in enumerate(all_ids):
        url = view_result_url_inc.format(e_id)
        r = client.get(url)
        if r.status_code == 200:
            try:
                election_name = get_election_name(r)
                table = get_result_table(r)
                es_1, es_2, es_3 = get_election_schedule(r)
                prepare_pdf(
                    election_name.contents[0],
                    es_1, es_2, es_3,
                    url,
                    table)
            except Exception as e:
                print('something went wrong, the error was:', e)

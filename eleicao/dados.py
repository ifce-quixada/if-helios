reitor_candidates = [
    'Geraldo Fernando Gonçalves de Freitas',
    'João Medeiros Tavares Junior',
    'Jose Wally Mendonça Menezes',
    'Júlio César da Costa Silva',
    'Voto Branco/Nulo'
]

elections = [
    {'short_name_campus': 'acarau', 'campus': 'Acaraú',
        'candidates': {
            'diretor': [
                'João Vicente Mendes',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'acopiara', 'campus': 'Acopiara',
        'candidates': {
            'diretor': [
                'Antonio Indalécio Feitosa',
                'Kelvio Felipe dos Santos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'aracati', 'campus': 'Aracati',
     'candidates': {
         'diretor': [
             'Márcia de Negreiros Viana',
             'Mario Wedney de Lima Moreira',
                'Voto Branco/Nulo'
            ]
         }
    },

    {'short_name_campus': 'baturite', 'campus': 'Baturité',
        'candidates': {
            'diretor': [
                'Fabiana dos Santos Lima',
                'Lourival Soares de Aquino Filho',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'boaviagem', 'campus': 'Boa Viagem',
        'candidates': {
            'diretor': [
                'João Paulo Arcelino do Rego',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'camocim', 'campus': 'Camocim',
        'candidates': {
            'diretor': [
                'Gilson Soares Cordeiro',
                'Heitor Hermeson de Carvalho Rodrigues',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'caninde', 'campus': 'Canindé',
        'candidates': {
            'diretor': [
                'Francisco Antônio Barbosa Vidal',
                'Michael Santos Duarte',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'caucaia', 'campus': 'Caucaia',
        'candidates': {
            'diretor': [
                'Cícero Antônio Maia Cavalcante',
                'Jefferson Queiroz Lima',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'cedro', 'campus': 'Cedro',
        'candidates': {
            'diretor': [
                'Antony Gleydson Lima Bastos',
                'Francisco José de Lima',
                'Saulo de Lima Bezerra',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'crateus', 'campus': 'Crateús',
        'candidates': {
            'diretor': [
                'Antonio Avelar Macedo Neri',
                'Antonio Marcos de Sousa Lima',
                'José Aglodualdo Holanda Cavalcante Júnior',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'crato', 'campus': 'Crato',
        'candidates': {
            'diretor': [
                'Francisco Messias Alves Filho',
                'Francisete Pereira Fernandes',
                'Joaquim Rufino Neto',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'fortaleza', 'campus': 'Fortaleza',
        'candidates': {
            'diretor': [
                'Adeildo Cabral da Silva',
                'Carlos Aurelio Oliveira Gonçalves',
                'José Eduardo Souza Bastos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'guaramiranga', 'campus': 'Guaramiranga',
        'candidates': {
            'diretor': [
                'Não tem candidato a diretor'
            ]
        }
    },

    {'short_name_campus': 'horizonte', 'campus': 'Horizonte',
        'candidates': {
            'diretor': [
                'Antônio Moisés Filho de Oliveira Mota',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'iguatu', 'campus': 'Iguatu',
        'candidates': {
            'diretor': [
                'Francisco Heber da Silva',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'itapipoca', 'campus': 'Itapipoca',
        'candidates': {
            'diretor': [
                'Fausto Faustino da Silva',
                'Marcelo Aguiar Távora',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'jaguaribe', 'campus': 'Jaguaribe',
        'candidates': {
            'diretor': [
                'Izamaro de Araújo',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'jaguaruana', 'campus': 'Jaguaruana',
        'candidates': {
            'diretor': [
                'Não tem candidato a diretor'
            ]
        }
    },

    {'short_name_campus': 'juazeiro', 'campus': 'Juazeiro do Norte',
        'candidates': {
            'diretor': [
                'Alex Jussileno Viana Bezerra',
                'Antonio Ismael Feitosa dos Santos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'limoeiro', 'campus': 'Limoeiro do Norte',
        'candidates': {
            'diretor': [
                'Arilene Franklin Chaves',
                'Francisco Valmir Dias Soares Junior',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'maracanau', 'campus': 'Maracanaú',
        'candidates': {
            'diretor': [
                'Rossana Barros Silveira',
                'Venicio Soares de Oliveira',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'maranguape', 'campus': 'Maranguape',
        'candidates': {
            'diretor': [
                'Robson da Silva Siqueira',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'morada', 'campus': 'Morada Nova',
        'candidates': {
            'diretor': [
                'Maria Beatriz Claudino Brandão',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'paracuru', 'campus': 'Paracuru',
        'candidates': {
            'diretor': [
                'Max William de Pinho Santana',
                'Toivi Masih Neto',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'pecem', 'campus': 'Pecém',
        'candidates': {
            'diretor': [
                'Cícero Antônio Maia Cavalcante',
                'Jefferson Queiroz Lima',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'quixada', 'campus': 'Quixadá',
        'candidates': {
            'diretor': [
                'Alexandre Cesar Praxedes Rodrigues',
                'Raimundo Aterlane Pereira Martins',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'reitoria', 'campus': 'Reitoria',
        'candidates': {
            'diretor': [
                'Não tem candidato a diretor'
            ]
        }
    },

    {'short_name_campus': 'sobral', 'campus': 'Sobral',
        'candidates': {
            'diretor': [
                'Wilton Bezerra de Fraga',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'tabuleiro', 'campus': 'Tabuleiro do Norte',
        'candidates': {
            'diretor': [
                'Francisco Sildemberny Souza dos Santos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'taua', 'campus': 'Tauá',
        'candidates': {
            'diretor': [
                'Alexciano de Sousa Martins',
                'José Alves de Oliveira Neto',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'tiangua', 'campus': 'Tianguá',
        'candidates': {
            'diretor': [
                'Jackson Nunes e Vasconcelos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'ubajara', 'campus': 'Ubajara',
        'candidates': {
            'diretor': [
                'Ulisses Costa de Vasconcelos',
                'Voto Branco/Nulo'
            ]
        }
    },

    {'short_name_campus': 'umirim', 'campus': 'Umirim',
        'candidates': {
            'diretor': [
                'Francisco Carlos de Sousa',
                'Maria Michele Colaço Pinheiro',
                'Voto Branco/Nulo'
            ]
        }
    },


]

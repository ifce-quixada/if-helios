#!/bin/env python

import os
import sys
import json
import uuid
import jinja2
import pdfkit
import requests
from bs4 import BeautifulSoup
from dados import elections

#ENV='dev'
ENV='prod'
CATEGORIES = ['Discente', 'Docente', 'Técnico-Administrativo']

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Content-Type': 'application/x-www-form-urlencoded'
}

config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
template_file = "templates/index.j2"

# alterar urls para apontar para sistema em producao
base_url =  'https://eleicoes.ifce.edu.br'
#base_url =  'https://eleicoes.ifce.edu.br'
auth_url = base_url + '/auth/ldap/login'
new_election_url = base_url + '/helios/elections/new'
elections_url = base_url + '/helios/elections/administered'

view_result_url_inc = base_url + '/helios/elections/{0}/view'

config_filename = '../auth.conf'
with open(config_filename, 'r') as config_file:
    auth_data = json.load(config_file)

release_result_data = {
    'csrf_token': ''
}

def get_session():
    client = requests.session()
    return client

def authenticate(client):
    r = client.post(auth_url, data = auth_data)
    return r, client

def get_csrf_token(client):
    r = client.get(new_election_url)
    soup = BeautifulSoup(r.text, 'lxml')
    csrf_token = soup.find('input', attrs = {'name': 'csrf_token'})['value']
    return csrf_token

def get_elections(client, url):
    r = client.get(url)
    return r

def get_elections_id(r):
    all_ids = []
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    for link in links:
        if link['href'].endswith('view'):
            all_ids.append(link['href'].split('/')[-2])
    return all_ids

def get_elections_cast_votes(r):
    all_cast_votes = []
    soup = BeautifulSoup(r.text, 'lxml')
    links = soup.find_all('a', href=True)
    links = [link.get_text() for link in links if link['href'].endswith('view')]
    ems = soup.find_all('em')
    ems = [em.get_text() for em in ems if em.get_text().endswith('votes')]
    all_cast_votes = dict(zip(links, ems))
    return all_cast_votes

def handle_cast_votes(d):
    new_d = { k: v for k, v in d.items() if 'Eleição' in k }
    #new_d = { k: v for k, v in d.items() if 'Comissão' in k }
    total_voters = { k: int(v.split('/')[0].split(' ')[2]) for k, v in new_d.items()}
    tae_casted_votes = { k: int(v.split('/')[1].split(' ')[1]) for k, v in new_d.items() if 'Técnico-Administrativo' in k}
    #tae_casted_votes = { k: int(v.split('/')[1].split(' ')[1]) for k, v in new_d.items() if 'TAE' in k}
    docente_casted_votes = { k: int(v.split('/')[1].split(' ')[1]) for k, v in new_d.items() if 'Docente' in k}
    discente_casted_votes = { k: int(v.split('/')[1].split(' ')[1]) for k, v in new_d.items() if 'Discente' in k}
    total = sum(tae_casted_votes.values()) + sum(docente_casted_votes.values()) + sum(discente_casted_votes.values())
    print('Votos - Técnico Administrativo', sum(tae_casted_votes.values()))
    print('Votos - Docente', sum(docente_casted_votes.values()))
    print('Votos - Discente', sum(discente_casted_votes.values()))
    print('Votos - todas as categorias', total)

def prepare_pdf(election_name, es_1, es_2, es_3, url, table_content):
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(template_file)
    outputText = template.render(
            election_name=election_name,
            es_1=es_1,
            es_2=es_2,
            es_3=es_3,
            url=url,
            table_content=table_content)
    short_name = election_name.replace(' ', '_').lower()
    pdfkit.from_string(
            outputText,
            os.path.join('pdfs/', short_name + '.pdf'),
            configuration=config,
            options= {
                'page-size': 'A4',
                'margin-top': '0.25in',
                'margin-right': '1in',
                'margin-left': '1in',
                'margin-bottom': '0.25in',
                'encoding': 'UTF-8'
            })
    print(short_name)


if __name__ == "__main__":
    client = get_session()
    r, client = authenticate(client)
    csrf_token = get_csrf_token(client)

    r = get_elections(client, elections_url)
    cast_votes = get_elections_cast_votes(r)
    handle_cast_votes(cast_votes)

    #for idx, e_id in enumerate(all_ids):
        #url = view_result_url_inc.format(e_id)
        #r = client.get(url)
        #if r.status_code == 200:
            #try:
                #election_name = get_election_name(r)
                #table = get_result_table(r)
                #es_1, es_2, es_3 = get_election_schedule(r)
                #prepare_pdf(
                    #election_name.contents[0],
                    #es_1, es_2, es_3,
                    #url,
                    #table)
            #except Exception as e:
            #    print('something went wrong, the error was:', e)
